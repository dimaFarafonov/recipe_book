import React from "react";
import ReactDOM from "react-dom";
import App from "../src/components/App";
import Modal from "react-modal";
Modal.setAppElement(document.getElementById("root"));
ReactDOM.render(<App />, document.querySelector("#root"));
