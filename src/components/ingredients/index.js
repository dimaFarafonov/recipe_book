import React, { Component } from "react";

export default class Ingredients extends Component {
  constructor(props) {
    super(props);
    this.state = {
      readOnly: true,
      Inputs: new Map(),
      ingredients: [
        [
          {
            name: "Lemon",
            count: 500,
          },
          {
            name: "Apple",
            count: 1000,
          },
          {
            name: "Potato",
            count: 1000,
          },
          {
            name: "Cheese",
            count: 500.6,
          },
          {
            name: "Onion1",
            count: 120,
          },
        ],
      ],
    };
  }

  ingredient = (index) => {
    return (
      <tr key={index}>
        <td>
          <input
            value={this.state.ingredients[this.props.index][index].name}
            readOnly={this.state.readOnly}
            ref={(input) => {
              this.state.Inputs[index] = input;
            }}
            onChange={(e) => {
              let ingredients = this.state.ingredients;
              ingredients[this.props.index][index].name = e.target.value;
              this.setState(ingredients);
            }}
          />
        </td>
        <td>
          <div
            style={{
              right: 10,
              flexDirection: "row",
              justifyContent: "space-between",
              display: "flex",
            }}
          >
            <input
              value={this.state.ingredients[this.props.index][index].count}
              onChange={(e) => {
                let ingredients = this.state.ingredients;
                ingredients[this.props.index][index].count = e.target.value;
                this.setState(ingredients);
              }}
              readOnly={this.state.readOnly}
              ref={(input) => {
                this.count = input;
              }}
            />
            <div>
              <button
                onClick={() => {
                  this.setState({ readOnly: false, index: index });
                  this.state.Inputs[index].focus();
                }}
              >
                E
              </button>
              <button
                onClick={() => {
                  this.state.ingredients[this.props.index].splice(index, 1);
                  this.setState(this.state);
                }}
              >
                D
              </button>
            </div>
          </div>
        </td>
      </tr>
    );
  };

  render() {
    if (this.props.index + 1 > this.state.ingredients.length)
      this.state.ingredients.push([{ name: "Ingredient", count: 0 }]);

    return (
      <div>
        <table style={{ width: 422, marginTop: 20 }}>
          <tbody>
            {this.state.ingredients[this.props.index].map((item, index) => {
              return this.ingredient(index);
            })}
          </tbody>
        </table>
        <button
          style={{ marginTop: 10 }}
          onClick={() => {
            this.state.ingredients[this.props.index].push({
              name: "Ingredient",
              count: 0,
            });
            this.setState(this.state);
          }}
        >
          Add ingredient
        </button>
      </div>
    );
  }
}
