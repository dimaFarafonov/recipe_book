import React, { Component } from "react";
import styles from "../../styles/CardProfile.module.css";
import Modal from "react-modal";
import cat_1 from "../../assets/cat_1.jpg";
import cat_2 from "../../assets/cat_2.jpg";
import cat_3 from "../../assets/cat_3.jpg";
import Ingredients from "../ingredients";
const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};
export default class CardProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [{ cat: cat_1 }, { cat: cat_2 }, { cat: cat_3 }],
    };
  }
  modal = () => {
    let { open, title, image, description } = this.state;
    return (
      <Modal
        isOpen={open}
        onRequestClose={() => this.setState({ open: false })}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <button onClick={() => this.setState({ open: false })}>Close</button>
        <button
          onClick={() =>
            this.props.editCard(this.props.index, title, image, description)
          }
        >
          Save
        </button>
        <div>Editing card_profile</div>
        <form>
          <div className={styles.card_profile}>
            <input
              value={title}
              onChange={(e) => {
                this.setState({ title: e.target.value });
              }}
            />
            <div className={styles.card_profile_first_nested}>
              {this.state.images.map((item, index) => {
                return (
                  <img
                    key={index}
                    onClick={() => {
                      alert(`You picked cat number ${index + 1}`);
                      this.setState({ image: item.cat });
                    }}
                    src={item.cat}
                    alt="logo"
                    className={[
                      styles.card_profile_image,
                      styles.card_popup_image,
                    ]}
                  />
                );
              })}
              <div className={styles.card_profile_description}>
                <input
                  value={description}
                  style={{ height: 40, width: "100%" }}
                  onChange={(e) => {
                    this.setState({ description: e.target.value });
                  }}
                />
              </div>
            </div>
          </div>
        </form>
      </Modal>
    );
  };
  render() {
    const { title, image, description } = this.props;
    return (
      <div
        style={{
          position: "absolute",
          right: 0,
          width: "50%",
          top: 0,
          height: 1000,
          paddingTop: 10,
        }}
      >
        <button
          onClick={() => {
            this.setState({ open: true });
            this.setState({
              title: title,
              image: image,
              description: description,
            });
          }}
        >
          Edit
        </button>
        <button
          style={{ marginLeft: 10 }}
          onClick={() => {
            this.props.deleteCard();
          }}
        >
          Delete
        </button>
        {this.modal(this.state.title, this.state.image, this.state.description)}
        <div className={styles.card_profile}>
          <h2>{title}</h2>
          <div className={styles.card_profile_first_nested}>
            <img src={image} alt="logo" className={styles.card_profile_image} />
            <div className={styles.card_profile_description}>{description}</div>
          </div>
        </div>
        <Ingredients index={this.props.index} />
      </div>
    );
  }
}
