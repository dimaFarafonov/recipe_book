import React, { Component } from "react";
import styles from "../../styles/Cards.module.css";
import default_image from "../../assets/default.jpg";
import CardProfile from "../card_profile/index";
export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      counter: [
        {
          title: "At mean mind 4",
          description:
            "Boy desirous families prepared gay reserved add ecstatic say. Replied joy age visitor nothing cottage. Mrs door paid led loud sure easy rea",
          image: default_image,
        },
      ],
    };
  }

  editCard = (index, title, image, description) => {
    let counter = this.state.counter;
    counter[index].title = title;
    counter[index].description = description;
    counter[index].image = image;
    this.setState(counter);
  };
  addCard = () => {
    let counter = this.state.counter;
    counter.push({
      title: "Title",
      description: "Description",
      image: default_image,
    });
    this.setState({ index: this.state.counter.length - 1, counter: counter });
  };
  deleteCard = () => {
    if (this.state.counter.length === 1) {
      alert("You cant delete last element");
    } else {
      let counter = this.state.counter;
      counter.splice(this.state.index, 1);
      this.setState({ index: this.state.counter.length - 1, counter: counter });
    }
  };
  card = (index, title, description, image) => {
    return (
      <div
        className={styles.main}
        onClick={() => this.setState({ index: index })}
        key={index}
      >
        <h2>{title}</h2>
        <div className={styles.main_first_nested}>
          <img src={image} alt="logo" className={styles.main_image} />
          <div className={styles.main_description}>{description}</div>
        </div>
      </div>
    );
  };
  render() {
    const { counter, index } = this.state;

    return (
      <div>
        {this.state.counter.map((item, index) => {
          return this.card(index, item.title, item.description, item.image);
        })}
        <button
          style={{ marginTop: 10 }}
          onClick={() => {
            this.addCard();
          }}
        >
          Add card
        </button>
        <CardProfile
          editCard={this.editCard}
          deleteCard={this.deleteCard}
          title={counter[index].title}
          image={counter[index].image}
          description={counter[index].description}
          index={index}
        />
      </div>
    );
  }
}
